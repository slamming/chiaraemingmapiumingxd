
Lane[] l = new Lane[3]; // PRENDE COME ARG LA Y DEL RETTANGOLO 


int posizione = 1; 

Player player;

void setup() {
  size(800, 600);
  player = new Player(750, 460);
  l[0] = new Lane(450);
  l[1] = new Lane(500);
  l[2] = new Lane(550);
}


void draw() {

  background(251, 125, 141);
  l[posizione].setVisibility(255);//TRASPARENZA 
  for (int i = 0; i < l.length; i++) {
    if (i!= posizione) {
      l[i].setVisibility(100);
    }
  }
  for (int i = 0; i < l.length; i++) {
    l[i].update(l, i);
  }
  for (int i = 0; i < l.length; i++) {
    l[i].show();
  }

  player.show();

  textSize(20);
  fill(0, 0, 0);
  //LOAD FONT AND CREATE FONT CERCA
  text("Distance: ", 10, 30); //da aggiungere distanza
}

void keyPressed() {


  if (key == 'a' && ((player.getX() - 10) > 0)) {
    player.updatePlayer(-10.0, 0);
  }

  if (key == 'd' && ((player.getX() + 32) < width)) {
    player.updatePlayer(+10.0, 0);
  }  

  if (key == 'w' && posizione > 0) {
    posizione --;
    player.updatePlayer(0, -50.0);
  }


  if (key == 's' && posizione < 2) {
    posizione++;
    player.updatePlayer(0, 50.0);
  }
}