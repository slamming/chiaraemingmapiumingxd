class Obstacle {
  float x, y, a;
  int altezza = 50;
  int larghezza = 50;

  Obstacle(float x, float y, float a) {
    this.x = x;
    this.y = y;
    this.a = a;
  }
  void update() {
    this.x --;
  }

  void show() {
    fill(251, 195, 141);
    rect(this.x, this.y - this.altezza, larghezza, altezza);
  }
}